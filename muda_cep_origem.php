/**
*   Retorna o CEP de um seller pelo ID
*/
function get_cep_seller($seller_id) {
    $address = get_user_meta($seller_id, 'dokan_profile_settings');
    return $address[0]['address']['zip'];
}

/**
*   Hook para alterar o CEP de origem do WooCommerce Correios
*
*   @param $cep_origem: CEP de origem. Deve ser alterado e retornado.
*   @param $metodo_entrega: correios_pac, correios_sedex, etc
*   @param $woocommerce_shipping_method_id: ID único do método de entrega dentro do WooCommerce
*   @param $carrinho: Um array com todos os itens do carrinho.
*/
function muda_cep_origem( $cep_origem, $metodo_entrega, $woocommerce_shipping_method_id, $carrinho ) {
    if (!empty($carrinho['seller_id']) && is_int($carrinho['seller_id'])) {
        $cep_origem = get_cep_seller($carrinho['seller_id']);
    } else {
	$id_produto = key($carrinho['contents']);
	$id_vendedor = get_post($id_produto);
	$cep_origem = get_cep_seller($id_vendedor->post_author);
    }
    return $cep_origem;
}
add_filter( 'woocommerce_correios_origin_postcode', 'muda_cep_origem', 10, 4 );